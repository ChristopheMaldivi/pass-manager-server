package org.tof.pass.links

import org.assertj.core.api.Assertions.*
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.web.reactive.server.WebTestClient
import org.tof.pass.pass.domain.LinkEntry
import org.tof.pass.pass.infra.sql.LinkEntryJPARepository

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class LinkDeletionTest(@Autowired val client: WebTestClient, @Autowired val repoJPA: LinkEntryJPARepository) {

    @BeforeEach
    @AfterEach
    fun clearRepo() {
        repoJPA.deleteAll()
    }

    @Test
    fun `returns bad request if uri query param is missing`() {
        // when
        client.delete()
                .uri("/links")
                .exchange()
                .expectStatus()
                .isBadRequest
    }

    @Test
    fun `returns ok if link was deleted`() {
        // given
        val uriToKeep = "https://linkToKeep"
        val linkEntryToKeep = LinkEntryForTest(uriToKeep)
        client.post()
                .uri("/links")
                .syncBody(linkEntryToKeep)
                .exchange()
                .expectStatus()
                .isOk
        assertThat(repoJPA.count()).isEqualTo(1)


        val expectedUri = "https://myUri-titi.org"
        val linkEntry = LinkEntryForTest(expectedUri)

        // when
        client.post()
                .uri("/links")
                .syncBody(linkEntry)
                .exchange()
                .expectStatus()
                .isOk
                .expectBody(LinkEntry::class.java)
        assertThat(repoJPA.count()).isEqualTo(2)
        assertThat(repoJPA.findById(expectedUri)).isPresent

        // when
        client.delete()
                .uri("/links?uri=$expectedUri")
                .exchange()
                .expectStatus()
                .isOk

        assertThat(repoJPA.count()).isEqualTo(1)
        assertThat(repoJPA.findById(uriToKeep)).isPresent
        assertThat(repoJPA.findById(expectedUri)).isNotPresent
    }

}
