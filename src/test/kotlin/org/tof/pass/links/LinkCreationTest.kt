package org.tof.pass.links

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.web.reactive.server.WebTestClient
import org.tof.pass.pass.domain.LinkEntry
import org.tof.pass.pass.infra.sql.LinkEntryJPARepository

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class LinkCreationTest(@Autowired val client: WebTestClient, @Autowired val repoJPA: LinkEntryJPARepository) {

    @BeforeEach
    @AfterEach
    fun clearRepo() {
        repoJPA.deleteAll()
    }

    @Test
    fun `returns bad request if link entry is missing`() {
        // when
        client.post()
                .uri("/links")
                .exchange()
                .expectStatus()
                .isBadRequest
    }

    @Test
    fun `returns bad request if uri is null`() {
        // given
        val linkEntry = LinkEntryForTest(null)

        // when
        client.post()
                .uri("/links")
                .syncBody(linkEntry)
                .exchange()
                .expectStatus()
                .isBadRequest
    }

    @Test
    fun `returns bad request if uri is empty`() {
        // given
        val linkEntry = LinkEntryForTest("")

        // when
        client.post()
                .uri("/links")
                .syncBody(linkEntry)
                .exchange()
                .expectStatus()
                .isBadRequest
    }

    @Test
    fun `returns ok if uri is not blank`() {
        // given
        val expectedUri = "myUri"
        val linkEntry = LinkEntryForTest(expectedUri)

        // when
        val actualLinkEntry = client.post()
                .uri("/links")
                .syncBody(linkEntry)
                .exchange()
                .expectStatus()
                .isOk
                .expectBody(LinkEntry::class.java)
                .returnResult()
                .responseBody

        assertThat(actualLinkEntry).isNotNull
        actualLinkEntry?.apply {
            assertThat(LinkEntryForTest(expectedUri)).isEqualTo(linkEntry)
        }
    }
}
