package org.tof.pass.links

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.reactive.server.WebTestClient
import org.tof.pass.pass.domain.LinkEntry
import org.tof.pass.pass.infra.sql.LinkEntryJPARepository

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class LinkListTest(@Autowired val client: WebTestClient, @Autowired val repoJPA: LinkEntryJPARepository) {

    @BeforeEach
    @AfterEach
    fun clearRepo() {
        repoJPA.deleteAll()
    }

    @Test
    fun `returns a list which contains previously created link entry`() {
        // given
        val expectedUri = "myUri"
        val linkEntry = LinkEntryForTest(expectedUri)
        client.post()
                .uri("/links")
                .syncBody(linkEntry)
                .exchange()
                .expectStatus()
                .isOk

        // when
        val readLinkEntries = client.get()
                .uri("/links")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus()
                .isOk
                .expectBodyList(LinkEntry::class.java)
                .returnResult()
                .responseBody

        // then
        assertThat(readLinkEntries).isNotNull
        assertThat(readLinkEntries).hasSize(1)
        readLinkEntries?.get(0).apply {
            assertThat(LinkEntryForTest(expectedUri)).isEqualTo(linkEntry)
        }
    }

    @Test
    fun `returns a list ordered by creation date, but reversed, last first`() {
        val uri1 = "uri1"
        val uri2 = "uri2"
        val uri3 = "uri3"
        val link1 = LinkEntryForTest(uri1)
        val link2 = LinkEntryForTest(uri2)
        val link3 = LinkEntryForTest(uri3)
        client.post().uri("/links").syncBody(link1).exchange().expectStatus().isOk
        client.post().uri("/links").syncBody(link2).exchange().expectStatus().isOk
        client.post().uri("/links").syncBody(link3).exchange().expectStatus().isOk

        // when
        val readLinkEntries = client.get()
                .uri("/links")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus()
                .isOk
                .expectBodyList(LinkEntry::class.java)
                .returnResult()
                .responseBody

        // then
        assertThat(readLinkEntries).isNotNull
        assertThat(readLinkEntries).hasSize(3)
        assertThat(readLinkEntries?.map{it.uri}).containsExactly(uri3, uri2, uri1)
    }
}
