package org.tof.pass.pass

data class PasswordEntryForTest(val uri: String?, val login: String?, val password: String?)