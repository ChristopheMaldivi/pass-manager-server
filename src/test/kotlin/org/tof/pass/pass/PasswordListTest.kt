package org.tof.pass.pass

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.reactive.server.WebTestClient
import org.tof.pass.pass.domain.PasswordEntry
import org.tof.pass.pass.infra.sql.PasswordEntryJPARepository

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class PasswordListTest(@Autowired val client: WebTestClient, @Autowired val repoJPA: PasswordEntryJPARepository) {

    @BeforeEach
    @AfterEach
    fun clearRepo() {
        repoJPA.deleteAll()
    }

    @Test
    fun `returns a list which contains previously created password entry`() {
        // given
        val expectedUri = "myUri"
        val expectedLogin = "myLogin"
        val expectedPassword = "myPassword"
        val passwordEntry = PasswordEntryForTest(expectedUri, expectedLogin, expectedPassword)
        client.post()
                .uri("/passwords")
                .syncBody(passwordEntry)
                .exchange()
                .expectStatus()
                .isOk

        // when
        val readPasswordEntries = client.get()
                .uri("/passwords")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus()
                .isOk
                .expectBodyList(PasswordEntry::class.java)
                .returnResult()
                .responseBody

        // then
        assertThat(readPasswordEntries).isNotNull
        assertThat(readPasswordEntries).hasSize(1)
        readPasswordEntries?.get(0).apply {
            assertThat(PasswordEntryForTest(expectedUri, expectedLogin, expectedPassword)).isEqualTo(passwordEntry)
        }
    }
}
