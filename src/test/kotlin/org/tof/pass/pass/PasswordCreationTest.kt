package org.tof.pass.pass

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.web.reactive.server.WebTestClient
import org.tof.pass.pass.domain.PasswordEntry
import org.tof.pass.pass.infra.sql.PasswordEntryJPARepository

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class PasswordCreationTest(@Autowired val client: WebTestClient, @Autowired val repoJPA: PasswordEntryJPARepository) {

    @BeforeEach
    @AfterEach
    fun clearRepo() {
        repoJPA.deleteAll()
    }

    @Test
    fun `returns bad request if password entry is missing`() {
        // when
        client.post()
                .uri("/passwords")
                .exchange()
                .expectStatus()
                .isBadRequest
    }

    @Test
    fun `returns bad request if uri is null`() {
        // given
        val passwordEntry = PasswordEntryForTest(null, "myLogin", "myPassword")

        // when
        client.post()
                .uri("/passwords")
                .syncBody(passwordEntry)
                .exchange()
                .expectStatus()
                .isBadRequest
    }

    @Test
    fun `returns bad request if login is null`() {
        // given
        val passwordEntry = PasswordEntryForTest("myUri", null, "myPassword")

        // when
        client.post()
                .uri("/passwords")
                .syncBody(passwordEntry)
                .exchange()
                .expectStatus()
                .isBadRequest
    }

    @Test
    fun `returns bad request if password is null`() {
        // given
        val passwordEntry = PasswordEntryForTest("myUri", "myLogin", null)

        // when
        client.post()
                .uri("/passwords")
                .syncBody(passwordEntry)
                .exchange()
                .expectStatus()
                .isBadRequest
    }

    @Test
    fun `returns bad request if uri is empty`() {
        // given
        val passwordEntry = PasswordEntryForTest("", "myLogin", "myPassword")

        // when
        client.post()
                .uri("/passwords")
                .syncBody(passwordEntry)
                .exchange()
                .expectStatus()
                .isBadRequest
    }

    @Test
    fun `returns bad request if login is empty`() {
        // given
        val passwordEntry = PasswordEntryForTest("myUri", "", "myPassword")

        // when
        client.post()
                .uri("/passwords")
                .syncBody(passwordEntry)
                .exchange()
                .expectStatus()
                .isBadRequest
    }

    @Test
    fun `returns bad request if password is empty`() {
        // given
        val passwordEntry = PasswordEntryForTest("myUri", "myLogin", "")

        // when
        client.post()
                .uri("/passwords")
                .syncBody(passwordEntry)
                .exchange()
                .expectStatus()
                .isBadRequest
    }

    @Test
    fun `returns ok if uri+login+password are not blank`() {
        // given
        val expectedUri = "myUri"
        val expectedLogin = "myLogin"
        val expectedPassword = "myPassword"
        val passwordEntry = PasswordEntryForTest(expectedUri, expectedLogin, expectedPassword)

        // when
        val actualPasswordEntry = client.post()
                .uri("/passwords")
                .syncBody(passwordEntry)
                .exchange()
                .expectStatus()
                .isOk
                .expectBody(PasswordEntry::class.java)
                .returnResult()
                .responseBody

        assertThat(actualPasswordEntry).isNotNull
        actualPasswordEntry?.apply {
            assertThat(PasswordEntryForTest(expectedUri, expectedLogin, expectedPassword)).isEqualTo(passwordEntry)
        }
    }
}
