package org.tof.pass.pass.domain

interface PasswordEntryRepository {
    suspend fun create(passwordEntry: PasswordEntry): PasswordEntry?
    suspend fun readAll(): List<PasswordEntry>
}