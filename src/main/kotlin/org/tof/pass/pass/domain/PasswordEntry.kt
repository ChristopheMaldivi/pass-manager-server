package org.tof.pass.pass.domain

data class PasswordEntry(val uri: String, val login: String, val password: String) {

    fun contentIsValid(): Boolean =
            uri.isNotBlank() && login.isNotBlank() && password.isNotBlank()

    suspend fun save(repo: PasswordEntryRepository): PasswordEntry? =
            repo.create(this)

    companion object {
        suspend fun list(repo: PasswordEntryRepository): List<PasswordEntry> =
                repo.readAll()
    }
}