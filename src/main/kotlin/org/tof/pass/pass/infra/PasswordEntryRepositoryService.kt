package org.tof.pass.pass.infra

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.tof.pass.pass.domain.PasswordEntry
import org.tof.pass.pass.domain.PasswordEntryRepository
import org.tof.pass.pass.infra.sql.PasswordEntryEntity
import org.tof.pass.pass.infra.sql.PasswordEntryJPARepository

@Service
class PasswordEntryRepositoryService(
        @Autowired val repoJPA: PasswordEntryJPARepository
) : PasswordEntryRepository {

    override suspend fun create(passwordEntry: PasswordEntry): PasswordEntry? {
        return repoJPA.save(PasswordEntryEntity.fromDomainObject(passwordEntry))
                .toDomainObject()
    }

    override suspend fun readAll(): List<PasswordEntry> {
        return repoJPA.findAll()
                .map { it.toDomainObject() }
    }
}