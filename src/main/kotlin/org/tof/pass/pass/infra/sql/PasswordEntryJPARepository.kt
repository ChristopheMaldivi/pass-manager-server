package org.tof.pass.pass.infra.sql

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface PasswordEntryJPARepository : CrudRepository<PasswordEntryEntity, String>