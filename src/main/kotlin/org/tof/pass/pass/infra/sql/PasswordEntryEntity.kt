package org.tof.pass.pass.infra.sql

import org.tof.pass.pass.domain.PasswordEntry
import javax.persistence.Entity
import javax.persistence.Id

@Entity
data class PasswordEntryEntity(
        @Id
        val uri: String,
        val login: String,
        val password: String) {

    fun toDomainObject() = PasswordEntry(uri, login, password)

    companion object {
        fun fromDomainObject(p: PasswordEntry) = PasswordEntryEntity(p.uri, p.login, p.password)
    }
}