package org.tof.pass.pass.domain

data class LinkEntry(val uri: String) {

    fun contentIsValid(): Boolean =
            uri.isNotBlank()

    suspend fun save(repo: LinkEntryRepository): LinkEntry? =
            repo.create(this)

    companion object {
        suspend fun list(repo: LinkEntryRepository): List<LinkEntry> =
                repo.readAllOrderedByCreationDateDesc()
    }
}