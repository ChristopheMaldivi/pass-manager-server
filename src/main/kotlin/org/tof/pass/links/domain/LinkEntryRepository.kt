package org.tof.pass.pass.domain

interface LinkEntryRepository {
    suspend fun create(linkEntry: LinkEntry): LinkEntry?
    suspend fun readAllOrderedByCreationDateDesc(): List<LinkEntry>
    suspend fun delete(uri: String)
}