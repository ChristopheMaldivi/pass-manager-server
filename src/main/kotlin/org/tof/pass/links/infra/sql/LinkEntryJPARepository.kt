package org.tof.pass.pass.infra.sql

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface LinkEntryJPARepository : CrudRepository<LinkEntity, String>