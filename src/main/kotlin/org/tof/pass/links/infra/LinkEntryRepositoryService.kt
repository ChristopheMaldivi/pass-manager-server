package org.tof.pass.pass.infra

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.tof.pass.pass.domain.LinkEntry
import org.tof.pass.pass.domain.LinkEntryRepository
import org.tof.pass.pass.infra.sql.LinkEntity
import org.tof.pass.pass.infra.sql.LinkEntryJPARepository

@Service
class LinkEntryRepositoryService(
        @Autowired val repoJPA: LinkEntryJPARepository
) : LinkEntryRepository {
    override suspend fun create(linkEntry: LinkEntry): LinkEntry? {
        return repoJPA.save(LinkEntity.fromDomainObject(linkEntry))
                .toDomainObject()
    }

    override suspend fun readAllOrderedByCreationDateDesc(): List<LinkEntry> {
        return repoJPA.findAll()
                .map { it.toDomainObject() }
                .reversed()
    }

    override suspend fun delete(uri: String) =
            repoJPA.deleteById(uri)
}