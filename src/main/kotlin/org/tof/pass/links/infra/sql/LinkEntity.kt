package org.tof.pass.pass.infra.sql

import org.tof.pass.pass.domain.LinkEntry
import javax.persistence.Entity
import javax.persistence.Id

@Entity
data class LinkEntity(
        @Id
        val uri: String) {

    fun toDomainObject() = LinkEntry(uri)

    companion object {
        fun fromDomainObject(p: LinkEntry) = LinkEntity(p.uri)
    }
}