package org.tof.pass

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.server.*
import org.springframework.web.reactive.function.server.ServerResponse.*
import org.tof.pass.pass.domain.LinkEntry
import org.tof.pass.pass.domain.LinkEntryRepository
import org.tof.pass.pass.domain.PasswordEntry
import org.tof.pass.pass.domain.PasswordEntryRepository

@Configuration
class Router(@Autowired val passwordsRepository: PasswordEntryRepository, @Autowired val linksRepository: LinkEntryRepository) {

    @Bean
    fun api() = coRouter {
        POST("/passwords") { createNewPassword(it) }
        GET("/passwords") { retrievePasswords() }

        POST("/links") { createNewLink(it) }
        GET("/links") { retrieveLinks() }
        DELETE("/links") { deleteLink(it) }
    }

    private suspend fun createNewPassword(req: ServerRequest): ServerResponse {
        val passwordEntry: PasswordEntry? = req.awaitBodyOrNull()
        passwordEntry?.apply {
            if (contentIsValid()) {
                save(passwordsRepository)?.let {
                    return ok()
                            .contentType(MediaType.APPLICATION_JSON)
                            .bodyAndAwait(it)
                }
                return status(HttpStatus.INTERNAL_SERVER_ERROR).buildAndAwait()
            }
        }

        return badRequest().buildAndAwait()
    }

    private suspend fun retrievePasswords(): ServerResponse {
        return ok()
                .contentType(MediaType.APPLICATION_JSON)
                .bodyAndAwait(PasswordEntry.list(passwordsRepository))
    }

    private suspend fun createNewLink(req: ServerRequest): ServerResponse {
        val linkEntry: LinkEntry? = req.awaitBodyOrNull()
        linkEntry?.apply {
            if (contentIsValid()) {
                save(linksRepository)?.let {
                    return ok()
                            .contentType(MediaType.APPLICATION_JSON)
                            .bodyAndAwait(it)
                }
                return status(HttpStatus.INTERNAL_SERVER_ERROR).buildAndAwait()
            }
        }

        return badRequest().buildAndAwait()
    }

    private suspend fun retrieveLinks(): ServerResponse {
        return ok()
                .contentType(MediaType.APPLICATION_JSON)
                .bodyAndAwait(LinkEntry.list(linksRepository))
    }

    private suspend fun deleteLink(req: ServerRequest): ServerResponse {
        val uriOptional = req.queryParam("uri")
        // FIXME REFACTO
        var uri: String?
        if (uriOptional.isPresent) {
            uri = uriOptional.get()
        } else {
            uri = null
        }
        uri?.let {
            linksRepository.delete(uri)
            return ok().buildAndAwait()
        }
        return badRequest().buildAndAwait()
    }
}