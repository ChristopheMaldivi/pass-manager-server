#!/bin/sh

docker kill $(docker ps -q)
docker run --rm -e POSTGRES_USER=tof -e POSTGRES_DB=pass -p 5432:5432 postgres &
