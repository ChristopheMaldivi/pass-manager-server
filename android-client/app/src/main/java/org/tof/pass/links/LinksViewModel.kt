package org.tof.pass.links

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import org.tof.pass.links.api.Link

class LinksViewModel(application: Application) : AndroidViewModel(application) {
    private val repository = LinksRepository(application)

    val links: LiveData<List<Link>> = repository.getLinks()

    fun refreshLinks() = repository.refreshLinks()
}