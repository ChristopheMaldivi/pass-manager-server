package org.tof.pass.links


import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.URLUtil
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_links.view.*
import org.tof.pass.R
import org.tof.pass.links.api.Link


class LinksRecyclerViewAdapter(private val activity: FragmentActivity, viewModel: LinksViewModel) :
    RecyclerView.Adapter<LinksRecyclerViewAdapter.ViewHolder>() {

    private val links = mutableListOf<Link>()

    init {
        viewModel.links.observe(activity, Observer { links -> linksUpdated(links) })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.fragment_links, parent, false)
        view.setOnClickListener { showUri(it.tag as String?) }
        return ViewHolder(view)
    }

    private fun showUri(uri: String?) {
        if (URLUtil.isHttpUrl(uri) || URLUtil.isHttpsUrl(uri)) {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
            startActivity(activity, browserIntent, null)
        } else {
            Log.e(TAG, "Can not launch uri, not http(s) '$uri'")
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val link = links[position]
        holder.uri.text = link.uri
        holder.itemView.tag = link.uri
    }

    override fun getItemCount(): Int = links.size

    private fun linksUpdated(links: List<Link>) =
        with(this.links) {
            clear()
            addAll(links)
            notifyDataSetChanged()
        }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val uri: TextView = view.uri
    }

    companion object {
        val TAG: String = LinksRecyclerViewAdapter::class.java.simpleName
    }
}
