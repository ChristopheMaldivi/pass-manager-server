package org.tof.pass.links.api

data class Link(val uri: String)