package org.tof.pass

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*
import org.tof.pass.links.LinksFragment
import org.tof.pass.links.LinksRepository

class MainActivity : AppCompatActivity() {

    private val linksFragment: Fragment = LinksFragment()
    private val fm = supportFragmentManager
    private val onNavigationItemSelectedListener = setupNavigationListener()

    private var active = linksFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupBottomNavigation()
        setupActionBar()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId) {
            R.id.action_settings -> startActivity(Intent(this, SettingsActivity::class.java))
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setupBottomNavigation() {
        nav_view.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
        setupFragments()
    }

    private fun setupFragments() {
        fm.beginTransaction().add(R.id.main_container, active, "links").commit()
    }

    private fun setupActionBar() {
        setSupportActionBar(toolbar as Toolbar?)
    }

    private fun setupNavigationListener(): BottomNavigationView.OnNavigationItemSelectedListener {
        return BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_links -> {
                    fm.beginTransaction().hide(active).show(linksFragment).commit()
                    active = linksFragment
                }
            }
            false
        }
    }
}
