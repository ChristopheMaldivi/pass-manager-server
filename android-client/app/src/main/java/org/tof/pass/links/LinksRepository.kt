package org.tof.pass.links

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.preference.PreferenceManager
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.tof.pass.R
import org.tof.pass.links.api.Link
import org.tof.pass.links.api.LinksApi
import org.tof.pass.links.api.RetrofitBuilder
import retrofit2.HttpException

data class LinksRepository(private val context: Context) {
    private val linksLive = MutableLiveData<List<Link>>()
    private var links = emptyList<Link>()

    fun refreshLinks() {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                val api = buildApi()
                api?.let {
                    links = it.getLinksAsync().await()
                    linksLive.postValue(links)
                }
            } catch (e: HttpException) {
                displayError(e)
            }
        }
    }

    fun getLinks(): LiveData<List<Link>> {
        refreshLinks()
        return linksLive
    }

    fun createLink(uri: String) {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                val api = buildApi()
                api?.let {
                    val link = it.createLinkAsync(Link(uri)).await()
                    Log.i(TAG, "New link created on server: '$link'")
                }
            } catch (e: HttpException) {
                displayError(e)
            }
        }
    }

    private suspend fun displayError(e: HttpException) {
        withContext(Dispatchers.Main) {
            val errMsg = "Failed to reach server, status '${e.code()}', message '${e.message()}''}"
            Toast.makeText(context, errMsg, Toast.LENGTH_LONG).show()
        }
    }

    private fun buildApi(): LinksApi? {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        val serverUrl = sharedPref.getString("server-url", context.getString(R.string.pref_server_url_hint))
        serverUrl?.let {
            return RetrofitBuilder.build(serverUrl)?.create(LinksApi::class.java)
        }
        return null
    }

    companion object {
        val TAG: String = javaClass::class.java.simpleName
    }
}