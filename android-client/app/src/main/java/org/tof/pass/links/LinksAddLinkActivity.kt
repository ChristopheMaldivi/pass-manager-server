package org.tof.pass.links

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.webkit.URLUtil.isHttpUrl
import android.webkit.URLUtil.isHttpsUrl
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import org.tof.pass.R

class LinksAddLinkActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_links_add_link)

        handleIntent(intent)?.let { uri ->
            createNewLinkOnServer(uri)
        }
        finish()
    }

    private fun handleIntent(intent: Intent?) =
        retrieveHttpLink(intent)

    private fun retrieveHttpLink(intent: Intent?): String? {
        intent?.apply {
            val isSendAction = action == Intent.ACTION_SEND
            val isTextPlain = type == "text/plain"
            if (!isSendAction || !isTextPlain) {
                displayError("Invalid intent, action '$action', type '$type'")
                return null
            }

            val uri = getStringExtra(Intent.EXTRA_TEXT)
            if (uri != null) {
                if (isHttpUrl(uri) || isHttpsUrl(uri)) {
                    return uri
                } else {
                    displayError("Invalid uri, uri '$action'")
                }
            } else {
                displayError("Invalid intent, no text found")
            }
        }
        return null
    }

    private fun displayError(error: String) {
        Log.e(TAG, error)
        Toast.makeText(this, error, Toast.LENGTH_LONG).show()
    }

    private fun createNewLinkOnServer(uri: String) {
        LinksRepository(this).createLink(uri)
        Toast.makeText(this, getString(R.string.link_created), Toast.LENGTH_SHORT).show()
    }

    companion object {
        val TAG: String = javaClass::class.java.simpleName
    }
}
