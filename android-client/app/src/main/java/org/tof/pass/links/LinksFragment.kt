package org.tof.pass.links

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import org.tof.pass.R

class LinksFragment : Fragment() {

    private var viewModel: LinksViewModel? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_links_list, container, false) as RecyclerView
        val activity = activity ?: return null

        view.apply {
            viewModel = ViewModelProviders.of(activity).get(LinksViewModel::class.java)
            viewModel?.let {
                layoutManager = LinearLayoutManager(context)
                adapter = LinksRecyclerViewAdapter(activity, it)
            }
        }
        return view
    }

    override fun onResume() {
        super.onResume()
        viewModel?.apply { refreshLinks() }
    }
}

