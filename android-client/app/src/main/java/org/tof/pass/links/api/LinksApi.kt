package org.tof.pass.links.api

import kotlinx.coroutines.Deferred
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface LinksApi {

    @GET("/link/links")
    fun getLinksAsync(): Deferred<List<Link>>

    @POST("/link/links")
    fun createLinkAsync(@Body link: Link): Deferred<Link>
}